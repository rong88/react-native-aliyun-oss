import { NativeModules } from 'react-native';

type AliyunOssType = {
  initWithSigner(endPoint: string, signUrl: string): void;
  asyncUpload(bucket: string, key: string, path: string): Promise<number>;
  asyncUploadPublic(bucket: string, key: string, path: string): Promise<number>;
  SUCCESS: number;
  ERROR: number;
};

const { AliyunOss } = NativeModules;

export default AliyunOss as AliyunOssType;
