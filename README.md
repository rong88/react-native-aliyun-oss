# react-native-aliyun-oss

aliyun oss api using in react native

## Installation

```sh
yarn add react-native-aliyun-oss
```

## Usage

```js
import AliyunOss from 'react-native-aliyun-oss';

// ...

AliyunOss.initWithSigner('<your endPoint>', '<your signUrl>');
const res = await AliyunOss.upload('<bucket>', '<key>', '<path>');
if (res === AliyunOss.SUCCESS) {
  //do something
}
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
