import * as React from 'react';

import { StyleSheet, View, Text } from 'react-native';
import AliyunOss from 'rn-aliyun-oss';
import SyanImagePicker from 'react-native-syan-image-picker';

export default function App() {
  React.useEffect(() => {
    AliyunOss.initWithSigner(
      'https://oss-cn-beijing.aliyuncs.com',
      'https://schedule.ourtramy.com/file/sign'
    );
  }, []);

  const handlePress = async () => {
    SyanImagePicker.showImagePicker(
      {
        imageCount: 1,
      },
      (err, selectedPhotos) => {
        if (err) {
          return;
        }

        upload(selectedPhotos[0].uri);
      }
    );
  };

  const upload = async (path: string) => {
    console.log('上传路径：' + path);
    // 选择成功，上传图片
    const start = Date.now();

    //注意IOS不需要删除这个文件头
    const res = await AliyunOss.asyncUpload(
      'tramy-schedule',
      'test_ios.png',
      path.replace('file://', '')
    );

    console.log('上传结果：' + res + ',耗时：' + (Date.now() - start));
  };

  return (
    <View style={styles.container}>
      <Text onPress={handlePress}>ClickToTest</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
